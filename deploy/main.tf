terraform {
  backend "s3" {
    bucket         = "poc-recipe-app-terraform-states"
    key            = "recipe-app.tfstate"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "poc-recipe-app-terraform-state-lock"
  }
}

provider "aws" {
  region  = "eu-west-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}