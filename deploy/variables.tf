variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipte-app-api-devops"
}

variable "contact" {
  default = "sjoerd.dejong@naturalis.nl"
}

variable "db_username" {
  description = "Username for the postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "sjoerd.dejong"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "538789244504.dkr.ecr.eu-west-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "538789244504.dkr.ecr.eu-west-1.amazonaws.com/poc-recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "DNS Domain name"
  default     = "dryrun.link"
}
variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api.raad",
    staging    = "api.staging.raad",
    dev        = "api.dev.raad"
  }
}