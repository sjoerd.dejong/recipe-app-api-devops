output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  value = aws_route53_record.app.fqdn
}

output "s3_public_files_bucket" {
  value = aws_s3_bucket.app_public_files.id
}